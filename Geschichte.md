Die Spengerstory

Kapitel 1: Es war einmal

Kapitel 2: Zwischen den Zeilen

Kapitel 3: Wer war dabei

Kapitel 4: Und dabei waren

Kapitel 5: Unerwarteter Besuch

Kapitel 6: Große Erwartungen

Kapitel 7: Enttäuschte Hoffnung

Kapitel 8: Schere Stein Papier

Kapitel 9: Zitterpartie

Kapitel 10: Nomen est Omen

Kapitel 11: Über die Konsole spricht man nicht

Kapitel 12: Auf der Pinwand

Kapitel 13: Staatliche Kontrolle

Kapitel 14: DNA und mehr

Kapitel 15: Autosuche

Kapitel 16: Programmieren zahlt sich aus

Kapitel 17: Rollmeter rollen nicht

Kapitel 18: Mobile Katzen putzen auch die Zähne

Kapitel 19: Mit Händen und Füßen

Kapitel 20: Von Kopf bis Fuß

